/* thumbs manager */
(function(window){
	
	var FWDUVPYoutubeScreen = function(prt, volume){
		
		var _s = this;
		var prototype = FWDUVPYoutubeScreen.prototype;
		
		_s.videoHolder_do = null;
		_s.ytb = null;
		
		_s.lastQuality_str = "auto";
		
		_s.volume = volume;
		
		_s.updateVideoId_int;
		_s.updatePreloadId_int;
		
		_s.controllerHeight = prt.data.controllerHeight;
		_s.hasHours_bl = false;
		_s.hasBeenCreatedOnce_bl = false;
		_s.allowScrubing_bl = false;
		_s.hasError_bl = false;
		_s.isPlaying_bl = false;
		_s.isStopped_bl = true;
		_s.isStartEventDispatched_bl = false;
		_s.isSafeToBeControlled_bl = false;
		_s.isPausedInEvent_bl = true;
		_s.isShowed_bl = true;
		_s.isQualityArrayDisapatched_bl = false; _s.playsinline = prt.data.playsinline ? 1 : 0;
		_s.isMbl = FWDUVPUtils.isMobile;
		
		//###############################################//
		/* init */
		//###############################################//
		_s.init = function(){
			
			_s.hasTransform3d_bl = false;
			_s.hasTransform2d_bl = false;
			_s.setBkColor("#000");
			_s.setBackfaceVisibility();
			prt.videoHolder_do.addChildAt(_s, 0);
			_s.resizeAndPosition();
			_s.setupVideo();
		};
	
		//###############################################//
		/* Setup youtube video */
		//##############################################//
		_s.setupVideo = function(){
		
			_s.videoHolder_do = new FWDUVPDisplayObject("div");
			_s.videoHolder_do.hasTransform3d_bl = false;
			_s.videoHolder_do.hasTransform2d_bl = false;
			_s.videoHolder_do.screen.setAttribute("id", prt.instanceName_str + "youtube");
			_s.videoHolder_do.getStyle().width = "100%";
			_s.videoHolder_do.getStyle().height = "100%";
			_s.videoHolder_do.setBackfaceVisibility();
			_s.addChild(_s.videoHolder_do);
			
			_s.ytb = new YT.Player(prt.instanceName_str + "youtube", {
				width:"100%",
				height:"100%",
				playerVars:{
					controls:0,
					disablekb:0,
					loop:0,
					autoplay:0,
					wmode:"opaque",
					showinfo:0,
					rel:0,
					modestbranding:1,
					iv_load_policy:3,
					cc_load_policy :0,
					fs:0,
					html5:1,
					playsinline:_s.playsinline
			  	},
			  	events: {
			  		"onReady":_s.playerReadyHandler,
			  		"onError":_s.playerErrorHandler,
			  		"onStateChange":_s.stateChangeHandler,
			  		"onPlaybackQualityChange":_s.qualityChangeHandler
			  	}
		    });
			_s.setBkColor("#FFFFFF")
			
		};
			
		_s.playerReadyHandler = function(){
			_s.resizeAndPosition();
			if(prt.data.aom_bl) _s.ytb.mute();
			_s.dispatchEvent(FWDUVPYoutubeScreen.READY);
			_s.hasBeenCreatedOnce_bl = true;
		};
		
		_s.stateChangeHandler = function(e){
			//logger.log(e.data + " " + prt.instanceName_str + " " + _s.isCued_bl)
			if(e.data == -1 && _s.isCued_bl && _s.isMbl){
				_s.isStopped_bl = false;
				FWDUVPlayer.stopAllVideos(prt);
			}
			
			if(e.data == YT.PlayerState.PLAYING){
				if(!_s.isSafeToBeControlled_bl){
					_s.isStopped_bl = false;
					_s.isSafeToBeControlled_bl = true;
					_s.isPlaying_bl = true;
					_s.hasHours_bl = Math.floor(_s.ytb.getDuration() / (60 * 60)) > 0;
					_s.setVolume(prt.volume);
					_s.startToUpdate();
					_s.startToPreload();
					_s.scrub(0.00001);
					if(!_s.isMbl) _s.setQuality(_s.lastQuality_str);
					
					if(_s.ytb.getAvailableQualityLevels() && _s.ytb.getAvailableQualityLevels().length != 0){
						_s.dispatchEvent(FWDUVPYoutubeScreen.QUALITY_CHANGE, {qualityLevel:_s.ytb.getPlaybackQuality(), levels:_s.ytb.getAvailableQualityLevels()});
					}
					_s.startToUpdateSubtitles();
				    _s.dispatchEvent(FWDUVPYoutubeScreen.SAFE_TO_SCRUBB);
				}
				if(_s.isPausedInEvent_bl) _s.dispatchEvent(FWDUVPYoutubeScreen.PLAY);
				_s.isPausedInEvent_bl = false;
				_s.hasError_bl = false;
				
			}else if(e.data == YT.PlayerState.PAUSED){
				if(!_s.isSafeToBeControlled_bl) return;
				_s.isStopped_bl = false;
				if(!_s.isPausedInEvent_bl) _s.dispatchEvent(FWDUVPYoutubeScreen.PAUSE);
				_s.isPausedInEvent_bl = true;
			}else if(e.data == YT.PlayerState.ENDED){
				if(_s.ytb.getCurrentTime() && _s.ytb.getCurrentTime() > 0){
					_s.isStopped_bl = false;
					_s.stopToUpdateSubtitles();
					setTimeout(function(){_s.dispatchEvent(FWDUVPYoutubeScreen.PLAY_COMPLETE);}, 100);
				}
			}else if(e.data == YT.PlayerState.CUED){
				if(!_s.isStopped_bl){
					_s.dispatchEvent(FWDUVPYoutubeScreen.CUED);
				}
				/*
				if(!prt.isMbl && (prt.data.autoPlay_bl || prt.isThumbClick_bl) && !_s.hasStarted_bl){
					prt.play();
					_s.hasStarted_bl = true;
				}
				*/
				_s.isCued_bl = true;
			}
		};
		
		_s.qualityChangeHandler = function(e){
			if(_s.ytb.getAvailableQualityLevels() && _s.ytb.getAvailableQualityLevels().length != 0){
				_s.dispatchEvent(FWDUVPYoutubeScreen.QUALITY_CHANGE, {qualityLevel:_s.ytb.getPlaybackQuality()});
			}
		};
		
		_s.playerErrorHandler = function(e){
			_s.isPausedInEvent_bl = true;
			if(_s.isStopped_bl || _s.hasError_bl) return;
			var error_str = "";
			_s.hasError_bl = true;
			if(e.data == 2){
				error_str = "The youtube id is not well formatted, make sure it has exactly 11 characters and that it dosn't contain invalid characters such as exclamation points or asterisks.";
			}else if(e.data == 5){
				error_str = "The requested content cannot be played in an HTML5 player or another error related to the HTML5 player has occurred.";
			}else if(e.data == 100){
				error_str = "The youtube video request was not found, probably the video ID is incorrect.";
			}else if(e.data == 101 || e.data == 150){
				error_str = "The owner of the requested video does not allow it to be played in embedded players.";
			}
			_s.dispatchEvent(FWDUVPYoutubeScreen.ERROR, {text:error_str});
		};
		
		//##############################################//
		/* Resize and position */
		//##############################################//
		_s.resizeAndPosition = function(){
			_s.setWidth(prt.tempVidStageWidth);
			_s.setHeight(prt.tempVidStageHeight);
			
			if(_s.videoHolder_do){
				_s.videoHolder_do.setWidth(prt.tempVidStageWidth);
				_s.videoHolder_do.setHeight(prt.tempVidStageHeight);
			
				if(_s.ytb && _s.ytb.a){
					try{
						_s.ytb.a.width = prt.tempVidStageWidth;
						_s.ytb.a.height = prt.tempVidStageHeight;
						_s.ytb.a.style.width = prt.tempVidStageWidth + "px";
						_s.ytb.a.style.height = prt.tempVidStageHeight + "px";
					}catch(e){}
				}
			}
			
		};
		
		//##############################################//
		/* Set path */
		//##############################################//
		_s.setSource = function(sourcePath){
			if(sourcePath) _s.sourcePath_str = sourcePath;
			clearInterval(_s.setSourceId_int);
			_s.setSourceId_int = setInterval(function(){
				if(_s.ytb.cueVideoById && _s.ytb.setPlaybackRate){
					_s.ytb.cueVideoById(_s.sourcePath_str);
					
					if(!prt.isMbl 
						&& (prt.data.autoPlay_bl || prt.isThumbClick_bl || (prt.isAdd_bl && !prt.loadAddFirstTime_bl))
						&& !prt.lightBox_do
						|| (prt.lightBox_do && prt.lightBox_do.isShowed_bl)
					){
						prt.videoPoster_do.hide(true);
						prt.lrgPlayBtn.hide();
						prt.play();
					}
					
					clearInterval(_s.setSourceId_int);
				}
			},50);
			
			//_s.isStopped_bl = false;
		};
	
		//##########################################//
		/* Play / pause / stop methods */
		//##########################################//
		_s.play = function(overwrite){
			FWDUVPlayer.curInstance = prt;
			_s.isPlaying_bl = true;
			_s.hasError_bl = false;
			_s.hasStarted_bl = true;
			try{
				_s.ytb.playVideo();
				_s.startToUpdate();
			}catch(e){}
			_s.isStopped_bl = false;
		};

		_s.pause = function(){
			if(_s.isStopped_bl || _s.hasError_bl) return;
			_s.isPlaying_bl = false;
			try{
				_s.ytb.pauseVideo();
			}catch(e){}
			_s.stopToUpdate();
		};
		
		_s.togglePlayPause = function(){
			if(_s.isPlaying_bl){
				_s.pause();
			}else{
				_s.play();
			}
		};
		
		_s.resume = function(){
			if(_s.isStopped_bl) return;
			_s.play();
		};
		
		//###########################################//
		/* Updates ... */
		//###########################################//
		_s.startToUpdate = function(){
			clearInterval(_s.updateVideoId_int);
			_s.updateVideoId_int = setInterval(_s.updateVideo, 500);
		};
		
		_s.stopToUpdate = function(){
			clearInterval(_s.updateVideoId_int);
		};
		
		_s.updateVideo = function(){
			var percentPlayed; 
			if(!_s.ytb){
				stopToUpdate();
				return;
			}
			if (!_s.allowScrubing_bl) {
				percentPlayed = _s.ytb.getCurrentTime() /_s.ytb.getDuration();
				_s.dispatchEvent(FWDUVPYoutubeScreen.UPDATE, {percent:percentPlayed});
			}
			
			var totalTime = _s.formatTime(_s.ytb.getDuration());
			var curTime = _s.formatTime(_s.ytb.getCurrentTime());
			
			_s.dispatchEvent(FWDUVPYoutubeScreen.UPDATE_TIME, {curTime:curTime , totalTime:totalTime, seconds:_s.ytb.getCurrentTime(), totalTimeInSeconds:_s.ytb.getDuration()});
		};
		
		_s.startToPreload = function(){
			clearInterval(_s.preloadVideoId_int);
			_s.updatePreloadId_int = setInterval(_s.updateProgress, 500);
		};
		
		_s.stopToPreload = function(){
			clearInterval(_s.updatePreloadId_int);
		};
		
		_s.updateProgress = function(){
			if(!_s.ytb){
				stopToPreload();
				return;
			}
			var buffered;
			var percentLoaded = _s.ytb.getVideoLoadedFraction();
			
			_s.dispatchEvent(FWDUVPYoutubeScreen.LOAD_PROGRESS, {percent:percentLoaded});
		};
		
		//###########################################//
		/* Event handlers */
		//###########################################//	
		_s.stop = function(){
			if(_s.isStopped_bl) return;
			//if(logger) logger.log("# YTB stop #" + prt.instanceName_str);
			_s.isPlaying_bl = false;
			_s.isStopped_bl = true;
			_s.hasStarted_bl = false;
			_s.isCued_bl = false;
			clearInterval(_s.setSourceId_int);
			_s.allowScrubing_bl = false;
			_s.isSafeToBeControlled_bl = false;
			_s.isQualityArrayDisapatched_bl = false;
			_s.isPausedInEvent_bl = true;
			_s.stopToUpdateSubtitles();
			_s.stopToUpdate();
			_s.stopToPreload();
			_s.stopVideo();
			_s.dispatchEvent(FWDUVPYoutubeScreen.STOP);
			_s.dispatchEvent(FWDUVPYoutubeScreen.LOAD_PROGRESS, {percent:0});
			//_s.dispatchEvent(FWDUVPYoutubeScreen.UPDATE_TIME, {curTime:"00:00" , totalTime:"00:00"});
		};
		
		_s.destroyYoutube = function(){
			if(_s.videoHolder_do){
				_s.videoHolder_do.screen.removeAttribute("id", prt.instanceName_str + "youtube");
				_s.videoHolder_do.destroy();
				_s.videoHolder_do = null;
			}
			if(_s.ytb) _s.ytb.destroy();
			_s.ytb = null;
		};
		
		_s.stopVideo = function(){
			_s.ytb.cueVideoById(_s.sourcePath_str);
			//_s.ytb.seekTo(0);
			//_s.ytb.clearVideo();
			//_s.ytb.stopVideo();
		};
		
		_s.setPlaybackRate = function(rate){
			if(!_s.ytb || _s.isMbl) return;
			if(rate) _s.rate = rate;
			if(_s.ytb.setPlaybackRate){
				_s.ytb.setPlaybackRate(Number(_s.rate));
			}
		};
		
		//##################################################//
		/* Suntitles */
		//##################################################//
		_s.stopToUpdateSubtitles = function(){
			clearInterval(_s.startToUpdateSubtitleId_int);	
		}
		
		_s.startToUpdateSubtitles = function(){
			clearInterval(_s.startToUpdateSubtitleId_int);
			_s.startToUpdateSubtitleId_int = setInterval(_s.updateSubtitleHandler, 10);
		}
		
		_s.updateSubtitleHandler = function(){
			
			_s.dispatchEvent(FWDUVPYoutubeScreen.UPDATE_SUBTITLE, {curTime:_s.ytb.getCurrentTime()});
		}

		//###############################################//
		/* Scrub */
		//###############################################//
		_s.startToScrub = function(){
			if(!_s.isSafeToBeControlled_bl) return;
			_s.allowScrubing_bl = true;
		};
		
		_s.stopToScrub = function(){
			if(!_s.isSafeToBeControlled_bl) return;
			_s.allowScrubing_bl = false;
		};
		
		_s.scrubbAtTime = function(duration){
			if(!_s.isSafeToBeControlled_bl) return;
			_s.ytb.seekTo(duration);
		}
		
		_s.scrub = function(percent){
			if(!_s.isSafeToBeControlled_bl) return;
			_s.ytb.seekTo(percent * _s.ytb.getDuration());
		};
	
		//###############################################//
		/* Volume */
		//###############################################//
		_s.setVolume = function(vol){
			if(vol != undefined) _s.volume = vol;
			if(_s.ytb){
				_s.ytb.setVolume(vol * 100);
				if(vol) _s.ytb.unMute();
			}
		};
		
		//###############################################//
		/* set quality */
		//###############################################//
		_s.setQuality = function(quality){
			_s.lastQuality_str = quality;
			_s.ytb.setPlaybackQuality(quality);
		};
		
		_s.formatTime = function(secs){
			var hours = Math.floor(secs / (60 * 60));
			
		    var divisor_for_minutes = secs % (60 * 60);
		    var minutes = Math.floor(divisor_for_minutes / 60);

		    var divisor_for_seconds = divisor_for_minutes % 60;
		    var seconds = Math.ceil(divisor_for_seconds);
		    
		    minutes = (minutes >= 10) ? minutes : "0" + minutes;
		    seconds = (seconds >= 10) ? seconds : "0" + seconds;
		    
		    if(isNaN(seconds)) return "00:00";
			if(_s.hasHours_bl){
				 return hours + ":" + minutes + ":" + seconds;
			}else{
				 return minutes + ":" + seconds;
			}
		};
		
	
		_s.init();
	};

	/* set prototype */
	FWDUVPYoutubeScreen.setPrototype = function(){
		FWDUVPYoutubeScreen.prototype = new FWDUVPDisplayObject("div");
	};
	
	FWDUVPYoutubeScreen.UPDATE_SUBTITLE = "updateSubtitle";
	FWDUVPYoutubeScreen.READY = "ready";
	FWDUVPYoutubeScreen.ERROR = "error";
	FWDUVPYoutubeScreen.UPDATE = "update";
	FWDUVPYoutubeScreen.UPDATE_TIME = "updateTime";
	FWDUVPYoutubeScreen.SAFE_TO_SCRUBB = "safeToControll";
	FWDUVPYoutubeScreen.LOAD_PROGRESS = "loadProgress";
	FWDUVPYoutubeScreen.PLAY = "play";
	FWDUVPYoutubeScreen.PAUSE = "pause";
	FWDUVPYoutubeScreen.STOP = "stop";
	FWDUVPYoutubeScreen.PLAY_COMPLETE = "playComplete";
	FWDUVPYoutubeScreen.CUED = "cued";
	FWDUVPYoutubeScreen.QUALITY_CHANGE = "qualityChange";


	window.FWDUVPYoutubeScreen = FWDUVPYoutubeScreen;

}(window));