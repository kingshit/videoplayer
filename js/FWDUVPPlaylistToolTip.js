/* FWDUVPPlaylistToolTip */
(function (window){
var FWDUVPPlaylistToolTip = function(
			bkPath_str,
			pointerPath_str,
			fontColor_str,
			position_str,
			playlistToolTipMaxWidth
		){
		
		var _s = this;
		var prototype = FWDUVPPlaylistToolTip.prototype;
		
		_s.buttonRef_do = null;
		
		_s.bkPath_str = bkPath_str;
		_s.pointerPath_str = pointerPath_str;
		
		_s.text_do = null;
		_s.pointer_do = null;
	
		_s.fontColor_str = fontColor_str;
		_s.position_str = position_str;
	
		_s.id = -1;
		if(_s.position_str == "bottom"){
			_s.pointerWidth = 7;
			_s.pointerHeight = 4;
		}else{
			_s.pointerWidth = 4;
			_s.pointerHeight = 7;
		}
		_s.maxWidth = playlistToolTipMaxWidth;
		
		_s.showWithDelayId_to;
		
		_s.isMbl = FWDUVPUtils.isMobile;
		_s.isShowed_bl = true;
	
		//##########################################//
		/* initialize */
		//##########################################//
		_s.init = function(){
			_s.setOverflow("visible");
			_s.setupMainContainers();
			_s.hide();
			_s.getStyle().background = "url('" + _s.bkPath_str + "')";
			_s.getStyle().zIndex = 9999999999999;
		};
		
		//##########################################//
		/* setup main containers */
		//##########################################//
		_s.setupMainContainers = function(){
			_s.text_do = new FWDUVPDisplayObject("div");
			_s.text_do.hasTransform3d_bl = false;
			_s.text_do.hasTransform2d_bl = false;
			_s.text_do.setBackfaceVisibility();
			_s.text_do.setDisplay("inline-block");
			_s.text_do.getStyle().fontFamily = "Arial";
			_s.text_do.getStyle().fontSize= "12px";
			_s.text_do.getStyle().color = _s.fontColor_str;
			_s.text_do.getStyle().fontSmoothing = "antialiased";
			_s.text_do.getStyle().webkitFontSmoothing = "antialiased";
			_s.text_do.getStyle().textRendering = "optimizeLegibility";
			_s.text_do.getStyle().lineHeight = "16px";
			_s.text_do.getStyle().padding = "6px";
			_s.text_do.getStyle().paddingTop = "4px";
			_s.text_do.getStyle().paddingBottom = "4px";
			_s.text_do.getStyle().textAlign = "center";
			_s.addChild(_s.text_do);
			
			var pointer_img = new Image();
			pointer_img.src = _s.pointerPath_str;
			_s.pointer_do = new FWDUVPDisplayObject("img");
			_s.pointer_do.setScreen(pointer_img);
			_s.pointer_do.setWidth(_s.pointerWidth);
			_s.pointer_do.setHeight(_s.pointerHeight);
			_s.addChild(_s.pointer_do);
		};
		
		//##########################################//
		/* set label */
		//##########################################//
		_s.setLabel = function(label, id){
		
			if(_s.id != id){
				_s.setVisible(false);
				_s.text_do.getStyle().whiteSpace = "normal";
				_s.setWidth(_s.maxWidth);
				_s.text_do.setInnerHTML(label);
			}
		
			setTimeout(function(){
				if(_s == null) return;
					//if(_s.id != id){
						var textW = _s.text_do.screen.getBoundingClientRect().width;
						
						if(textW < 8 && textW != undefined){
							_s.setHeight(Math.round(_s.text_do.screen.getBoundingClientRect().height * 100));
							textW = Math.round(textW * 100);
						}else{
							_s.setHeight(_s.text_do.screen.offsetHeight);
							textW = Math.round(_s.text_do.screen.offsetWidth);
						}
						
						if(textW < _s.w - 15) _s.setWidth(textW);
					
						if(textW < _s.maxWidth){
							_s.text_do.getStyle().whiteSpace = "nowrap";
						}
						//}
					_s.positionPointer();
					_s.id = id;
					
				},60);
		
		};
		
		_s.positionPointer = function(offsetX){
			var finalX;
			var finalY;
			
			if(!offsetX) offsetX = 0;
			if(_s.position_str == "bottom"){
				finalX = parseInt((_s.w - _s.pointerWidth)/2) + offsetX;
				finalY = _s.h;
			}else{
				finalX = _s.w;
				finalY = parseInt((_s.h - _s.pointerHeight)/2);
			}
			
			_s.pointer_do.setX(finalX);
			_s.pointer_do.setY(finalY);
		};
		
		//##########################################//
		/* show / hide*/
		//##########################################//
		_s.show = function(){
			if(_s.isShowed_bl) return;
			_s.isShowed_bl = true;
			
			FWDAnimation.killTweensOf(_s);
			clearTimeout(_s.showWithDelayId_to);
			_s.showWithDelayId_to = setTimeout(_s.showFinal, 100);
		};
		
		_s.showFinal = function(){
			_s.setVisible(true);
			_s.setAlpha(0);
			FWDAnimation.to(_s, .4, {alpha:1, onComplete:function(){_s.setVisible(true);}, ease:Quart.easeOut});
		};
		
		_s.hide = function(){
			if(!_s.isShowed_bl) return;
			clearTimeout(_s.showWithDelayId_to);
			FWDAnimation.killTweensOf(_s);
			_s.setVisible(false);
			_s.isShowed_bl = false;
		};
		
		_s.init();
	};
	
	/* set prototype */
	FWDUVPPlaylistToolTip.setPrototype = function(){
		FWDUVPPlaylistToolTip.prototype = null;
		FWDUVPPlaylistToolTip.prototype = new FWDUVPDisplayObject("div", "fixed");
	};
	
	FWDUVPPlaylistToolTip.CLICK = "onClick";
	FWDUVPPlaylistToolTip.MOUSE_DOWN = "onMouseDown";
	
	FWDUVPPlaylistToolTip.prototype = null;
	window.FWDUVPPlaylistToolTip = FWDUVPPlaylistToolTip;
}(window));